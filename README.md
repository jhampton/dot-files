# dotfiles
Common dotfiles and a general guide to set up new environments

# Install homebrew

# Manual Usage
$ git clone https://gitlab.com/jhampton/dot-files $HOME/dotfiles

Create symbolic links:
```sh
  ln -s $HOME/dotfiles/.vimrc ~/.vimrc
  ln -s $HOME/dotfiles/.zshrc ~/.zshrc
  ln -s $HOME/dotfiles/.tmux.conf ~/.tmux.conf
 ```

# Setup

# Install Terminal
* Mac - [Iterm2](https://www.iterm2.com)
* Win - [ConEmu](https://conemu.github.io/)
* Nix - [Terminator](./scripts/install_terminator.sh)
    * Install Instructions:
    ```sh
    sudo add-apt-repository ppa:gnome-terminator
    sudo apt-get update
    sudo apt-get install terminator
    ```

# Install Zsh
* Nix - [Zsh](./scripts/install_zsh_nix.sh)
    * Install Instructions:
    ```sh
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install zsh
    ```
* Mac - [Zsh](./scripts/install_zsh_mac.sh)
   * Install Instructions:
   ```sh
   brew doctor
   brew install zsh
   ```
# Install Oh my Zsh
* [Oh my Z](./scripts/install_oh_my_zsh.sh)
* Current Theme "agnoster"

# Install Neo Vim
* Mac - [NeoVim](./scripts/install_neovim_mac.sh) 
    ```sh
    brew install neovim
    ```
* Win - [NeoVim](./scripts/install_neovim_win.sh)
    ```sh
    choco install neovim
    ```
* Nix - [NeoVim](./scripts/install_neovim_nix.sh)
   ```sh
   sudo apt-get install neovim
   ```
# Install Plugins
    ```sh
    nvim
    :PlugInstall
    ```

# Install ack 
* Mac
   ```sh
   brew install ack 
   ```
* Win 
   ```sh
   choco install ack 
   ```
* Nix
  ```sh
  sudo cpan install App:Ack
  ```

# Oh my Zsh theme
[Bullet Train](https://github.com/caiogondim/bullet-train.zsh)

# Install Python

`$ brew install python`

# Install Go

`$ brew install go`

# Install fnm

https://github.com/Schniz/fnm

https://github.com/starship/starship
